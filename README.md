# ABS Microservices framework
The ABS microservices framework is designed to to build microservices-based
software with the Software Product Line Engineering (SPLE) approach. This framework extends the capability
of Abstract Behavioral Specification (ABS) language for web development. 



There are two main directories:
- `framework` contains business logic implementation
- `middleware` contains library and auth management

The microservices message flow is shown in the following figure.

![Microservice Message Flow](docs/microservice-arch.png)

## How to Build and Run

### Build product 
Go to directory "<root>/framework", run "bash build.sh"

  * Output: ProductName.jar, that contains the generated product and all required libraries.

### Build middleware

Go to directory "<root>/middleware", run "ant"

  * Output "<root>/middleware/dist/middleware.jar"

### Run product
To run the generated product, execute: 
> java -jar <NamaProduk.jar> -p <port,any> <br />
> Go to http://localhost:[port], and then access the route (API) path.

